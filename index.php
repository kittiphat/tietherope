<?php
  define('FORM_LABEL_SPACE', 'col-sm-3');
  define('FORM_INPUT_SPACE', 'col-sm-9');
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Rosalin &amp; Peat - Wedding RSVP</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap-theme.min.css">
    <link href="//fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
    <style type="text/css">
      body {
        font-family: 'Dosis', sans-serif;
        font-weight: 300;
      }
      .logo {
        width: 100%;
        height: 0px;
        padding-bottom: 30%;    /* 450:1500 */;
        background-size: contain;
        background-image: url(assets/images/logo.png);
      }
      .logo-container {
        padding: 50px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
          <div class="logo-container">
            <div class="logo" style=""></div>
            <p class="text-center"><small><a href="https://www.facebook.com/events/606068033116128/" target="_blank">20 JAN 2018 10:30 ~ 14:00</a></small><br> <small><a href="https://www.google.co.th/maps/place/Nai+Lert+Heritage+Home/@13.7466255,100.5438967,17z/data=!3m1!4b1!4m5!3m4!1s0x30e29ec4a3cf918d:0x612c3b10fdde07fb!8m2!3d13.7466255!4d100.5460854?hl=en" target="_blank">NAI LERT PARK HERITAGE HOME, BANGKOK</a></small></p>
            <h3 class="text-center"><span style="font-weight: 500">Wedding RSVP</span></h3>
          </div>
          <p>Please support us by sparing your 2 minutes to provide us your address &amp; confirm your attendance.</p>
          <p>We are planning our special event to share with you, your quick response would mean so much to us.</p>
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-offset-3 col-sm-6">
          <form class="form-horizontal" method="POST" onsubmit="return validateForm();">
            <div class="form-group">
              <label for="knownname" class="<?= FORM_LABEL_SPACE ?> control-label">Attendee</label>
              <div class="<?= FORM_INPUT_SPACE ?>">
                <input type="text" name="knownname" id="knownname" class="form-control" placeholder="First Name + Last Name" required="required" />
              </div>
            </div>
            <div class="form-group">
              <label class="<?= FORM_LABEL_SPACE ?> control-label"></label>
              <div class="<?= FORM_INPUT_SPACE ?>">
                <div class="radio">
                  <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="yes" required="required">
                    <b>Accepts</b> with pleasure. Ready to see you getting married!
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="no">
                    <b>Declines</b> with regret. Will toast to you from afar.
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios3" value="unsure">
                    <b>Uncertain</b>. Please check with me again early January via Facebook messenger or Line..
                  </label>
                </div>
              </div>
            </div>
            <div class="attendable-info collapse">
              <hr>
              <div class="form-group">
                <label for="postal_address" class="<?= FORM_LABEL_SPACE ?> control-label">Address</label>
                <div class="<?= FORM_INPUT_SPACE ?>">
                  <textarea name="postal_address" id="postal_address" class="form-control lazy-required" rows="3" placeholder="Address"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="postal_code" class="<?= FORM_LABEL_SPACE ?> control-label">Postal Code</label>
                <div class="<?= FORM_INPUT_SPACE ?>">
                  <input type="text" name="postal_code" id="postal_code" class="form-control lazy-required" placeholder="Postal Code" />
                </div>
              </div>

              <hr>

              <p>Please introduce your plus 1 (or 2) who you will bring  to our wedding reception :)</p>
              <div class="form-group">
                <label for="plusone" class="<?= FORM_LABEL_SPACE ?> control-label">#1</label>
                <div class="<?= FORM_INPUT_SPACE ?>">
                  <input type="text" name="plusone" id="plusone" class="form-control" placeholder="Leave this blank &amp; get one at the wedding." />
                </div>
              </div>
              <div class="form-group">
                <label for="plustwo" class="<?= FORM_LABEL_SPACE ?> control-label">#2</label>
                <div class="<?= FORM_INPUT_SPACE ?>">
                  <input type="text" name="plustwo" id="plustwo" class="form-control" placeholder="Leave this blank &amp; get one at the wedding." />
                </div>
              </div>
            </div><!-- attendable info end -->

            <hr>
            <button id="submit" class="form-control">Submit</button>
            <p style="padding-bottom: 50px;"></p>
          </form>
        </div>
      </div>
    </div>

    <!-- POPUP -->
    <div id="thankYouModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">All done!</h4>
          </div>
          <div class="modal-body">
            <p>Thank you for your kind responses. Now we can proceed our special event preparation to the next step :)</p>
          </div>
          <div class="modal-footer">
            <button id="thankYouButton" type="button" class="btn btn-default" data-dismiss="modal">See you there!</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

    <!-- Optional theme -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(document).on('change', 'input[name="optionsRadios"]', function(e) {

          // show and hide more options.
          var selected = $('input[name="optionsRadios"]:checked').val();
          if ('yes' == selected) {
            $('.attendable-info').addClass('in');
            $('.attendable-info .lazy-required').attr('required', 'required');
            $('#thankYouButton').html('See you there!');
          }
          else {
            $('.attendable-info').removeClass('in');
            $('.attendable-info .lazy-required').removeAttr('required');
            $('#thankYouButton').html('Okay');
          }
        });
      });

      var clearForm = function() {
        $('#knownname').val('');
        $('#postal_address').val('');
        $('#postal_code').val('');
        $('#plusone').val('');
        $('#plustwo').val('');
        $('input[name="optionsRadios"]:checked').prop('checked', false).trigger('change');
      };

      var validateForm = function(event) {

        $('#submit').prop('disabled', true);

        // Submit via AJAX.
        $.ajax({
          url: 'https://docs.google.com/forms/d/e/1FAIpQLSeirWGIQ9Hz8w4ogMdb7VZ-jeOzJcx2o0GaBAwQ_J8_w8V-fA/formResponse',
          type: "POST",
          dataType: "xml",
          data: {
            'entry.1900407248': $('#knownname').val(),
            'entry.1025433305': $('input[name="optionsRadios"]:checked').val(),
            'entry.1321296793': $('#postal_address').val(),
            'entry.1258747433': $('#postal_code').val(),
            'entry.1700148368': $('#plusone').val(),
            'entry.88309370': $('#plustwo').val()
          }
        });

        // Submit via AJAX.
        $.ajax({
          url: 'http://peatiscoding.me/wp-admin/admin-ajax.php?method=write&action=do_tietherope_request',
          type: "POST",
          dataType: "jsonp",
          data: {
            'name': $('#knownname').val(), // name
            'status': $('input[name="optionsRadios"]:checked').val(),
            'postal_address': $('#postal_address').val(),
            'postal_code': $('#postal_code').val(),
            'plusone': $('#plusone').val(),
            'plustwo': $('#plustwo').val()
          },
          success: function(r) {
            console.log('data', r);
            // Show thank you message
            $('#thankYouModal').modal();
            $('#submit').prop('disabled', false);
            clearForm();
          },
          error: function(xhr, text, error) {
            console.log(xhr, text, error);
            $('#submit').prop('disabled', false);

            //
            alert('Unable to process request: ' + error);
          }
        });

        return false;
      };
    </script>
  </body>
</html>