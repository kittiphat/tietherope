<?php
/*
Plugin Name: Peat & Ros Wedding Guest List Viewer.
Description: Spring boarded with wordpress.
 */

function tietherope_table_name() {
	global $wpdb;

	return $wpdb->prefix.'tietherope_guestlist';
}

// Activation and installation.
function tietherope_install() {
	global $wpdb;

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	// Commission Table
	$table_name = tietherope_table_name();
	
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id bigint(20) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
		name varchar(250) NOT NULL,
		status enum('yes','no','unsure') NOT NULL DEFAULT 'unsure',
		postal_address text,
		postal_code varchar(20) DEFAULT NULL,
		plusone varchar(250) DEFAULT NULL,
		plustwo varchar(250) DEFAULT NULL,
		created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
	) {$charset_collate};";

	dbDelta( $sql );
}

register_activation_hook( __FILE__, 'tietherope_install' );

// Add Backend HTMLs

function tietherope_management() {
    add_menu_page('tietherope', 
    	'Tie the Rope', 
    	'administrator', 
    	basename(dirname(__FILE__)).'/admin/index.php', 
    	NULL,
    	'dashicons-avatar',
    	29
    );
}
add_action( 'admin_menu', 'tietherope_management' );

require_once 'ajax.php';