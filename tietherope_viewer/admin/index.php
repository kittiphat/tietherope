<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/handsontable/0.34.4/handsontable.full.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/handsontable/0.34.4/handsontable.full.min.css" />

<h1>#tietherope</h1>
<p class="description">Guest List</p>

<?php
	global $wpdb;

	$table = tietherope_table_name();

	$data = $wpdb->get_results("SELECT * FROM $table;", ARRAY_A);

	if (!empty($r->last_error)) {
		die($r->last_error);
	}
?>

<!-- dom -->
<p>POST your data to: <code><?= TietheRopeAjax::url('write') ?></code>
<div id="hot" style="height: 68vh; width: 100%;"></div>
<br><p class="description text-right">Server Time: <?= date('Y-m-d H:i:s') ?></p>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		var data = <?php echo json_encode($data); ?>; 

		// init hot.
		window.hot = Handsontable(document.getElementById('hot'), {
			data: data,
			height: '68vh',
			colHeaders: [
				"ID",
				"Name",
				"Status",
				"Address",
				"Postal Code",
				"Plus One",
				"Plus Two",
				"Created At"
			]
		});
	});
</script>