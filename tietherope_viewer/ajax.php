<?php
class TietheRopeAjax {

	static function url($method) {
		return admin_url('admin-ajax.php?method='.$method.'&action=do_tietherope_request');
	}

	function write() {
		$name = utl::required('name');
		$status = utl::required('status');
		$postal_address = utl::optional('postal_address');
		$postal_code = utl::optional('postal_code');
		$plusone = utl::optional('plusone');
		$plustwo = utl::optional('plustwo');

		// Write onto db
		global $wpdb;

		$table = tietherope_table_name();

		$r = $wpdb->insert($table, array(
			'name' 			=> $name,
			'status' 		=> $status,
			'postal_address' => $postal_address,
			'postal_code'	=> $postal_code,
			'plusone'		=> $plusone,
			'plustwo'		=> $plustwo
		));

		if ($r !== FALSE) {
			return $r;
		}
		else {
			throw new Exception('Unable to update requested parameter: '.$wpdb->last_error);
		}
	}
}

function do_tietherope_request($nopriv=TRUE) {

	$callback = utl::optional('callback');
	if ($callback) {
		header('content-type: application/javascript; charset=utf-8');
		$prefix = $callback.'(';
		$suffix = ");";
	}
	else {
		header('content-type: application/json; charset=utf-8');
		$prefix = "";
		$suffix = "";
	}
	try {
		$method = utl::required('method');
		$ajax = new TietheRopeAjax();
		if (!method_exists($ajax, $method)) {
			throw new Exception('Invalid method: '.$method);
		}
		$o = $ajax->$method();

		echo $prefix.json_encode([
			'success' => true,
			'data' => $o
		]).$suffix;
	}
	catch(Exception $e) {
		echo $prefix.json_encode([
			'success' => false,
			'error' => $e->getMessage()
		]).$suffix;
	}
	die();
}

// Wordpress hooks.
add_action('wp_ajax_do_tietherope_request', function() {
	do_tietherope_request(FALSE);
});
add_action('wp_ajax_nopriv_do_tietherope_request', function() {
	do_tietherope_request(TRUE);
});

// Utility
if (class_exists('Utl')) {
	return;
}
class Utl {

	static function required($name, $array = NULL) {
		if (is_null($array)) {
			$array = $_REQUEST;
		}
		if (array_key_exists($name, $array)) {
			return $array[$name];
		}
		throw new Exception(''.$name.' is required parameter.');
	}

	static function optional($name, $default=NULL, $array = NULL) {
		if (is_null($array)) {
			$array = $_REQUEST;
		}
		if (array_key_exists($name, $array)) {
			return $array[$name];
		}
		return $default;
	}
}